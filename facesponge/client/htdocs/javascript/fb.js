// facebook api stuff

function fb_interface(appid, runme)
{
    FB.init({appId: appid, status: true, cookie: true, xfbml: true});

    /////////////////////////////////////////////////////////////////
    // the login stuff

    // check to see if the user has logged in, and ask for login if they haven't
    this.init=function()
    {
        var fb=this;
        //facebook_status("connecting with facebook...");
        FB.getLoginStatus(function(response) {
            if (response.session) {
                //facebook_status("You are now naked on Pluto.");
                // we are already logged in
                runme();
            } else {
                // the login is a popup so need the user to click on a button
                // for the browser to allow this to happen
                facebook_status('<input id="use-button" type="button" value="Use Sponge" onClick="fb.login();">');
            }
        });
    }

    // do the login onto facebook
    this.login=function()
    {
        FB.login(function(response) {
            if (response.status=="connected") {
                //facebook_status("You are now naked on Pluto.");
                runme();
            } else {
                facebook_status(":( the login didn't work! " + '<input type="button" value="Login to facebook" onClick="login();">');
            }
        },{scope:'user_about_me,friends_about_me,user_activities,friends_activities,user_education_history,friends_education_history,user_events,friends_events,user_groups,friends_groups,user_hometown,friends_hometown,user_interests,friends_interests,user_likes,friends_likes,user_location,friends_location,user_notes,friends_notes,friends_website,user_work_history,friends_work_history,user_photos,friends_photos'});
    }

    this.init();
}
