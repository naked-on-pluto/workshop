
// converts a js object into text for displaying
function object_to_string(obj)
{
    return JSON.stringify(obj);
}

function choose(l) 
{
    return l[Math.floor(Math.random()*l.length)];
}

// add some html to the page
function display(html)
{ 
    var out=html;
    if (typeof html=='object') {
        out=JSON.stringify(html);
    }
    var div=document.createElement("div");   
    div.id = "foo";
    div.innerHTML = out;
    document.getElementById("output").appendChild(div);
}

function display_image(src)
{
    display('<img src='+src+'>');
}

// add some html to the page
function log_error(html)
{ 
    if ($('#content-errors').length == 0) {
        var div=document.createElement("div");
        div.id = "content-errors";
        div.innerHTML = '<h1>Oops...</h1>'
        document.getElementById("errors").appendChild(div);
    }
    var div=document.createElement("div");   
    div.id = "oopsie";
    div.innerHTML = html;
    document.getElementById("content-errors").appendChild(div);
}

// clear the page
function clear(id)
{
    var element=document.getElementById(id);
    while (element.firstChild) 
    {
        element.removeChild(element.firstChild);
    }
}

function facebook_status(html)
{
 //   clear('fb-status')
    var div=document.createElement("div");
    div.id = "foo";
    div.innerHTML = html;
    document.getElementById('fb-status').appendChild(div);
}
