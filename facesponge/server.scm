#!/usr//bin/env mzscheme
#lang scheme/base
;; Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
;;                                       
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require scheme/system
         scheme/foreign
         scheme/cmdline
         web-server/servlet
         web-server/servlet-env
         xml
         "scripts/server-interface.ss"
         "scripts/request.ss"
         "scripts/logger.ss")

; a utility to change the process owner,
; assuming mzscheme is called by root.
(unsafe!)
(define setuid (get-ffi-obj 'setuid #f (_fun _int -> _int)))

(define registered-requests
  (list
   (register (req 'project '(name)) project)
   (register (req 'new-project '(name)) new-project)
   ;(register (req 'edit-script '(project)) edit-script)
   (register (req 'write-script '(project code save cancel)) write-script)))

(define (start request)
  (let ((values (url-query (request-uri request))))
    (if (not (null? values)) ; do we have some parameters?      
        (let ((name (assq 'function_name values)))
          (when name ; is this a well formed request?
                (request-dispatch
                 registered-requests             
                 (req (string->symbol (cdr name))
                      (filter
                       (lambda (v)
                         (not (eq? (car v) 'function_name)))
                       values)))))
        (main-page))))

(printf "server is running...~n")

; Here we become the user 'nobody'.
; This is a security rule that *only works* if nobody owns no other processes
; than mzscheme. Otherwise better create another dedicated unprivileged user.
; Note: 'nobody' must own the state directory and its files.

(setuid 65534)

;;

(serve/servlet start
               ; port number is read from command line as argument
               ; ie: ./server.scm 8080	
               #:port (string->number (command-line #:args (port) port))
               #:command-line? #t
               #:servlet-path server-path
               #:server-root-path
               (build-path "client"))

